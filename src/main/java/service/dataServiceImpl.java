package service;

import org.springframework.beans.factory.annotation.Autowired;

import delegate.dataDelegate;

public class dataServiceImpl implements dataService{
	
	@Autowired dataDelegate delegate;

	@Override
	public String getDataById(String Location, String Longitude) throws Exception {
		 Object data = null;
	        try {
	            data = delegate.getDataById(Location, Longitude);
	        } catch (Exception e) {
	        	System.out.println("Error en nivel servicio");
	        }
	        return (String) data;
	}

}
