package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import service.dataService;

@RestController
@RequestMapping("get")
public class dataController {
	
	@Autowired dataService service;

	@GetMapping("/suggestions")
	public String getData(@RequestParam(required = true) String Location,
			@RequestParam(required = true) String Longitude) {
		
		Object response = null;
        try {
            Object data = service.getDataById(Location, Longitude);
            List<Object> list = new ArrayList<>();
            
            response = list.add(data);
        } catch (Exception e) {
            response = "Algo fallo";
        }	
		
		return (String) response;
		
	}

}
